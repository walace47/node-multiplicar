const fs = require('fs');

let listar = async function(base, limite) {
    if (!Number(base)) throw `La base no es un numero`;
    let data = '';
    for (let i = 1; i <= limite; i++) {
        console.log(`${base} X ${i} = ${i * base}`);
    }
}

let crearArchivo = async function(base, limite) {
    if (!Number(base)) throw `La base no es un numero`;
    let data = '';
    for (let i = 1; i <= limite; i++) {
        data += `${base} X ${i} = ${i * base}\n`;
    }
    let vari = 'tabla-' + base + '.txt';

    fs.writeFile('./tablas/tabla-' + base + '.txt', data, (err) => {
        if (err) throw err;

        console.log('The file has been saved!');

    });

    return vari;

}
module.exports = {
    crearArchivo,
    listar
}