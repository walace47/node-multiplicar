const opt = {
    base: {
        demand: true,
        alias: 'b'
    },
    limite: {
        demand: true,
        alias: 'l',
        default: 10
    }
}


const argv = require('yargs')
    .command('listar', 'lista la tabla de multiplicar ', opt)
    .command('crear', 'crea un archivo de la tabla de multiplicar', opt)
    .help()
    .argv;

module.exports = {
    argv
}