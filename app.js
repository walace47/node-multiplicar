const { crearArchivo, listar } = require('./multiplicar/multiplicar');
const { argv } = require('./config/yargs');

/* let parametros = argv[2];
let base = parametros.split('=')[1]; */
console.log(argv._[0]);
let { base, limite } = argv;

switch (argv._[0]) {
    case 'listar':
        listar(base, limite);
        break
        break;
    case 'crear':
        crearArchivo(base, limite)
            .then((res) => { console.log(`Archivo ${res} creado`) })
            .catch((e) => console.log(e));
        break
    default:
        console.log('comando no reconocido')
        break;
}